//
//  client.c
//  
//
//  Created by Jeel on 11/12/14.
//
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>   /* needed for mkfifo */
#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>

#define BUFFERSIZE 1024
#define CHMOD 0666

int main (int argc, char **argv)
{
    char msg[BUFFERSIZE];
    int fifo1, fifo2;
    
    printf("opening fifo...\n");
    while(1)
    {
        
        fifo1 = open("chatServ", O_RDONLY);
        fifo2 = open("chatCli", O_WRONLY);
        
        printf(">> ");
        fgets(msg, BUFFERSIZE, stdin);
        //scanf("%s",msg);
        write(fifo2, msg, BUFFERSIZE);
        printf("waiting for server..\n");

       
        read(fifo1, msg, BUFFERSIZE);
        
        printf("Message from server: %s\n>>", msg);
        close(fifo1);
        
        close(fifo2);
        
    }
    return 0;
    exit(0);
}