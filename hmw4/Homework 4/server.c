//
//  server.c
//  
//
//  Created by Jeel on 11/12/14.
//
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>   /* needed for mkfifo */
#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>

#define BUFFERSIZE 1024
#define CHMOD 0666

int main (int argc, char **argv)
{
    char msg[BUFFERSIZE];
    int fifo1, fifo2;
    
    mkfifo("chatServ", CHMOD); // make FIFOS
    mkfifo("chatCli", CHMOD);
    
    while(1)
    {
        printf("waiting for client..\n");
        fifo1 = open("chatServ", O_WRONLY); // open and read from then.
        fifo2 = open("chatCli", O_RDONLY);
        
        read(fifo2, msg, BUFFERSIZE);
        printf("Message from client: %s\n>>", msg);
        fgets(msg, BUFFERSIZE, stdin);
        //scanf("%s",msg);
        write(fifo1, msg, BUFFERSIZE);
        close(fifo1);
        close(fifo2);
        
    }
    return 0;
    exit(0);
}