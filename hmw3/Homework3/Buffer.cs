﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
namespace Homework3
{
    class Buffer<T>
    {
        /*
         * Author: Jeel Patel
         * Created: Oct 11, 2014
         */
        private static int BUFFER_SIZE = 10000; //max size of buffer array
        private int count = 0; //number of items currently in the buffer
        private int inPos = 0;   // points to the next free position in the buffer
        private int outPos = 0;  // points to the first filled position in the buffer
        private Object[] buffer = new Object[BUFFER_SIZE]; //array 
       // private Semaphore mutex = new Semaphore(1, 1); //provides limited access to the buffer (mutual exclusion)
        private SpinLock _lock = new SpinLock();
        private Semaphore empty = new Semaphore(BUFFER_SIZE, BUFFER_SIZE); //keep track of the number of empty elements in the array
        private Semaphore full = new Semaphore(0, BUFFER_SIZE); //keep track of the number of filled elements in the array

        public void insert(Object item)
        {

            empty.WaitOne(); //keep track of number of empty elements (value--)
            var taken = false;
            _lock.Enter(ref taken);
            //mutex.WaitOne(); //mutual exclusion
            // add an item to the buffer
            ++count;
            buffer[inPos] = item;
            //modulus (%) is the remainder of a division
            //for example, 0%3=0, 1%3=1, 2%3=2, 3%3=0, 4%3=1, 5%3=2
            inPos = (inPos + 1) % BUFFER_SIZE;
            _lock.Exit();
            //mutex.Release(); //mutual exclusion
            full.Release(1);
        }

        public long remove()
        {
            Object item = null;

            full.WaitOne(); //keep track of number of elements (value--)
            //This provides synchronization for consumer, 
            //because this makes the Consumer stop running when buffer is empty
            var taken = false;
            _lock.Enter(ref taken);
            //mutex.WaitOne(); //mutual exclusion
            // remove an item from the buffer
            --count;
            item = buffer[outPos];
            outPos = (outPos + 1) % BUFFER_SIZE;
            _lock.Exit();
            //mutex.Release(); //mutual exclusion
            empty.Release(1); //keep track of number of empty elements (value++)      	  
            //if buffer was full, then this wakes up the Producer 
            return (long)item;
        }

        public int Size()
        {
            return buffer.Length;
        }

    }
}