//
//  hw2.c
//  
//
//  Created by Jeel on 9/28/14.
//  The data set does not include all of the measured times because we had talked in class about how an ssd will affect the write
//  speeds for this homework.
//
//

#include "hw2.h"
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <time.h>

#define SIZE (1024*1024*10) //different values
#define TOTAL (1024*1024*1000) //100 mb file
char BUFFER [SIZE];


int main(int argc, char* argv[]) {
    
    clock_t start, end;
    
    int output = open("file.txt", O_CREAT | O_TRUNC | O_RDWR, 0666); //create the file
    
    start = clock(); // time how long the function takes
    
    double writeCount = TOTAL/SIZE;
    
    int i=0;
    
    while (i < writeCount){
        write(output, &BUFFER[0], SIZE);
        i++;
    }
    
    end = clock(); //end time
    
    double time_used = ((double) (end - start)) / CLOCKS_PER_SEC; //calulate the time the function took.
        
    printf("The function took: %f seconds to execute\n", time_used); // print the time.
    
    close(output);
    return 0;
    
}